function ratio = ThresholdFinder( volume, seed, initLowThreshold, initHighThreshold)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

  H = size(volume, 1); % image height
  W = size(volume, 2); % image width
  D = size(volume, 3);
  
  lThreshold = initLowThreshold;
  hThreshold = initHighThreshold;
  
  ratio = zeros(256 - initHighThreshold + initLowThreshold + 1, 1); 
  %first increment hThreshold until it hits 255, then set hThreshold back
  %to initHighThreshold, then decrement initLowThreshold, until it hits 0;
  
  %first increment hThreshold
  idx = 1;
  order = (seed(3)-1)*H*W + (seed(2)-1) * H + seed(1);
  for hThreshold = initHighThreshold:255
    mask = ( volume <= hThreshold & volume >= lThreshold );
    cc = bwconncomp(mask);
    
    for i = 1:numel(cc.PixelIdxList)
      v = cc.PixelIdxList{i};
      if any( v == order)
        ratio(idx) = numel(v);
        break;
      end
    end
    idx = idx + 1;
    
  end
  
  hThreshold = initHighThreshold;
  
  %then decrement lThreshold
  for lThreshold = initLowThreshold:-1:0
    mask = ( volume <= hThreshold & volume >= lThreshold );
    cc = bwconncomp(mask);
    
    for i = 1:numel(cc.PixelIdxList)
      v = cc.PixelIdxList{i};
      if any( v == order)
        ratio(idx) = numel(v);
        break;
      end
    end
    idx = idx + 1;
  end

end

