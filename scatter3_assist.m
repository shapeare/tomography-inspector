function [ x, y, z ] = scatter3_assist( volume )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%   [R, C, D] = size(volume);
%   x = zeros( size(volume(:)) );
%   y = x;
%   z = x;
%   idx = 1;
%   for d=1:D
%     for r=1:R
%       for c=1:C
%         if volume(r, c, d) == 0
%           x(idx) = 0; y(idx) = 0; z(idx) = 0;
%         else
%           x(idx) = C - c + 1; y(idx) = R - r + 1; z(idx) = d;
%         end
%         idx = idx + 1;
%       end
%     end
%   end
%   x(x==0) = [];
%   y(y==0) = [];
%   z(z==0) = [];

  I = find(volume ~= 0);
  [y, x, z] = ind2sub(size(volume),I);
  x = size(volume, 2) - x;
  y = size(volume, 1) - y;
  
%   x = x - 1;
%   y = y - 1;
%   z = z - 1;
  if numel(x) > 2000
    select = randi(numel(x), [1 2000]);
  else
    select = [1:numel(x)];
  end
  x = x(select);
  y = y(select);
  z = z(select);
end
