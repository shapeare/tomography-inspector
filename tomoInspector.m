function varargout = tomoInspector(varargin)
% TOMOINSPECTOR MATLAB code for tomoInspector.fig
%      TOMOINSPECTOR, by itself, creates a new TOMOINSPECTOR or raises the existing
%      singleton*.
%
%      H = TOMOINSPECTOR returns the handle to a new TOMOINSPECTOR or the handle to
%      the existing singleton*.
%
%      TOMOINSPECTOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TOMOINSPECTOR.M with the given input arguments.
%
%      TOMOINSPECTOR('Property','Value',...) creates a new TOMOINSPECTOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tomoInspector_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tomoInspector_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tomoInspector

% Last Modified by GUIDE v2.5 22-Apr-2014 20:20:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tomoInspector_OpeningFcn, ...
                   'gui_OutputFcn',  @tomoInspector_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before tomoInspector is made visible.
function tomoInspector_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tomoInspector (see VARARGIN)

% Choose default command line output for tomoInspector
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes tomoInspector wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = tomoInspector_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
sliceth = uint16( get(hObject, 'Value') );
set(handles.sliceth, 'String', num2str( sliceth ));
hImage = imshow(handles.images(:,:,sliceth), 'Parent', handles.axes1);
set(hImage,'ButtonDownFcn', {@ax_bdfcn, handles});

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on button press in importImageBtn.
function importImageBtn_Callback(hObject, eventdata, handles)
% hObject    handle to importImageBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex] = uigetfile('*.*','MultiSelect','on');
numFrames = numel(FileName);
I = imread( strcat(PathName, FileName{1}) );
sequence = zeros([size(I) numFrames],class(I));
sequence(:,:,1) = I;
for p = 2:numFrames
    sequence(:,:,p) = imread( strcat(PathName, FileName{p}) ); 
end
handles.images = sequence;
hImage = imshow(sequence(:,:,1), 'Parent', handles.axes1);

handles.preVolume = false( size(sequence));
handles.cVolume = handles.preVolume;
axes(handles.axes2);
isosurface(handles.cVolume, 0);

set(hImage,'ButtonDownFcn', {@ax_bdfcn, handles});
set(handles.sliceStart, 'String', num2str(1));
set(handles.sliceEnd, 'String', num2str(numFrames));
set(handles.slider1, 'min', 1, 'max', numFrames);
set(handles.slider1, 'SliderStep', [1, 1] / (numFrames - 1));

%set(handles.axes2, 'xLim', [1, size(I,2)], 'yLim', [1, size(I,1)], 'zLim', [1, size(sequence, 3)]);
guidata(hObject, handles);

% --- Executes on button press in undoBtn.
function undoBtn_Callback(hObject, eventdata, handles)
% hObject    handle to undoBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.cVolume = handles.preVolume;
cla(handles.axes2);
axes(handles.axes2);
isosurface(handles.cVolume, 0);
guidata(hObject, handles);


function sliceth_Callback(hObject, eventdata, handles)
% hObject    handle to sliceth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sliceth as text
%        str2double(get(hObject,'String')) returns contents of sliceth as a double


% --- Executes during object creation, after setting all properties.
function sliceth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliceth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sliceStart_Callback(hObject, eventdata, handles)
% hObject    handle to sliceStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sliceStart as text
%        str2double(get(hObject,'String')) returns contents of sliceStart as a double


% --- Executes during object creation, after setting all properties.
function sliceStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliceStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sliceEnd_Callback(hObject, eventdata, handles)
% hObject    handle to sliceEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sliceEnd as text
%        str2double(get(hObject,'String')) returns contents of sliceEnd as a double


% --- Executes during object creation, after setting all properties.
function sliceEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliceEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function [] = ax_bdfcn(hObject, eventdata, handles)
%handles = guidata(hObject);  
   axesHandle  = get(hObject,'Parent');
   coordinates = get(axesHandle,'CurrentPoint'); 
   coordinates = coordinates(1,1:2);
   pPos = [round(coordinates(2)), round(coordinates(1)), round(get(handles.slider1, 'Value'))];
%p = get(hObject, 'CurrentPoint'); % Get the position of the mouse.
S.points(1) = line(coordinates(1), coordinates(2),'Marker','+');  % Make our plot.

%screen = zeros(size(handles.preVolume), class(handles.preVolume));
%screen(:,:, pPos(3)) = 255;
handles.preVolume = handles.cVolume;
handles.cVolume = handles.cVolume | magicwand3d( handles.images, str2double(get(handles.lThreshold, 'String')), str2double(get(handles.hThreshold, 'String')), pPos(1), pPos(2), pPos(3) );

axes(handles.axes2);
[x, y, z] = scatter3_assist( handles.cVolume );
scatter3(x, y, z, '*');

%smoothedV = smooth3( handles.cVolume );
%isosurface( smoothedV ,0);
%camlight; lighting gouraud

%[x, y, z] = meshgrid( 1:size(handles.cVolume, 2), 1:size(handles.cVolume, 1), 1:size(handles.cVolume, 3));
%scatter3(x(:), y(:), z(:), handles.cVolume(:));
guidata(hObject, handles);

%set(S.ln(1),'uicontextmenu',S.cm)  % So user can click a point too.

% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function toleranceVal_Callback(hObject, eventdata, handles)
% hObject    handle to toleranceVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of toleranceVal as text
%        str2double(get(hObject,'String')) returns contents of toleranceVal as a double
%disp(str2double(get(hObject,'String')));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function toleranceVal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to toleranceVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', num2str(8));
guidata(hObject, handles);


% --- Executes on button press in saveBtn.
function saveBtn_Callback(hObject, eventdata, handles)
% hObject    handle to saveBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filename = strcat('result', date, '.tif');
%imwrite( handles.cVolume, filename, 'compression', 'none');
for i = 1:size(handles.cVolume, 3)
  imwrite(handles.cVolume(:, :, i), filename, 'WriteMode', 'append',  'Compression','none');
end


% --- Executes on mouse press over axes background.
function axes2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function lThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to lThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lThreshold as text
%        str2double(get(hObject,'String')) returns contents of lThreshold as a double


% --- Executes during object creation, after setting all properties.
function lThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', num2str(255));
guidata(hObject, handles);


function hThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to hThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of hThreshold as text
%        str2double(get(hObject,'String')) returns contents of hThreshold as a double


% --- Executes during object creation, after setting all properties.
function hThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to hThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', num2str(255));
guidata(hObject, handles);