function     bin_mask = magicwand3d(im, lThreshold, hThreshold, r, c, d)

% A 3D version of region growing algorithm, inspired by Yoram Tal's 2D implementation.                                                
% (C) Xiaoxiong Xing: xingxiaoxiong@gmail.com


H = size(im, 1); % image height
W = size(im, 2); % image width
D = size(im, 3);

%Create the binary mask
color_mask = false(H, W, D);

g = double(im);
ref = double(im(r,c,d)); 
color_mask = color_mask | ( (g>=lThreshold) & (g<=hThreshold) );

cc = bwconncomp(color_mask);
segList = [];
idx = (d-1)*H*W + (c-1) * H + r;
for i = 1:numel(cc.PixelIdxList)
  v = cc.PixelIdxList{i};
  if any( v == idx)
    segList = [segList; i];
  end
end

%bin_mask = zeros(H, W, D, 'uint8');
%bin_mask( cc.PixelIdxList{segList(1)} ) = 255;

bin_mask = false(H, W, D);
if numel(segList) > 0
  bin_mask( cc.PixelIdxList{segList(1)} ) = 1;  
end


% Connected component labelling
% [objects, count] = bwlabel(color_mask, 8); 
% 
% [y x v] = find(objects);
% segList = [];
% 
% k = find(x == c(1) & y == r(1));
% segList = [segList; v(k)];
% 
% segList = unique(segList);
% 
% LUT = zeros(1,count+1);
% LUT(segList+1) = 1;
% bin_mask = LUT(objects+1);


% Output
% TAG = 'Binary image result of magicwand';
% obj  = findobj('tag',TAG);
% 
% if isempty(obj), 
%     h = figure;
%     set(h,'tag',TAG);
%     Name = ['Fig ', num2str(h), ': ', TAG];
%     set(h,'NumberTitle','off','Name',Name);
% else, 
%     figure(obj);
% end
% 
% clf
% imshow(bin_mask,'InitialMagnification','fit');
