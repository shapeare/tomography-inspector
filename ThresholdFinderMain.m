function ratio = ThresholdFinderMain( pathname, filename, startIdx, endIdx, seed, initL, initH)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
   fileIdxStr = sprintf('%04i.tif', startIdx);
   filepath = strcat(pathname, '/', filename, fileIdxStr);
   firstslice = imread( filepath );
   
   fileIdx = startIdx + 1;
   
   volume = zeros([size(firstslice) endIdx-startIdx+1], class(firstslice));
   volume(:,:,1) = firstslice;
   for p = 2:endIdx-startIdx+1
     fileIdxStr = sprintf('%04i.tif', fileIdx);
     fileIdx = fileIdx + 1;
     volume(:,:,p) = imread( strcat(pathname, '/', filename, fileIdxStr) ); 
   end
   
   ratio = ThresholdFinder(volume, seed, initL, initH);

end

